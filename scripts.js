var	eventsArray = [];
var	selectedIndex = -1;

function init() {
	document.getElementById("savedEvents").innerHTML = "";
	if (localStorage.eventsRecords) {
		eventsArray = JSON.parse(localStorage.eventsRecords);
		for (var i = 0; i < eventsArray.length; i++) {
		    read(i, eventsArray[i].eventname, eventsArray[i].eventdescription);
		}
	}
}

function create() {
	var eventName = document.getElementById("eventname").value;
    var eventDescription = document.getElementById("eventdescription").value;

    var eventRecord = {eventname: eventName, eventdescription: eventDescription};
    if (selectedIndex === -1) {
        eventsArray.push(eventRecord);
    } else {
        eventsArray.splice(selectedIndex, 1, eventRecord);
    }
    localStorage.eventsRecords = JSON.stringify(eventsArray);
    init();
    clearAfterFill();
}

function clearAfterFill() {
    selectedIndex = -1;
    document.getElementById("eventname").value = "";
    document.getElementById("eventdescription").value = "";
    document.getElementById("submit").innerHTML = "Submit";
}

function read(index, eventName, eventDescription) {
	var table = document.getElementById("savedEvents");
                var row = table.insertRow();
                var eventNameCell = row.insertCell(0);
                var eventDescriptionCell = row.insertCell(1);
                var actionCell = row.insertCell(2);
                eventNameCell.innerHTML = eventName;
                eventDescriptionCell.innerHTML = eventDescription;
                actionCell.innerHTML = '<button onclick="update(' + index + ')">Edit</button><button onclick="removeRow(' + index + ')">Delete</button>';
}
function removeRow(index) {
    eventsArray.splice(index, 1);
    localStorage.eventsRecords = JSON.stringify(eventsArray);
    init();
}

function update(index) {
    selectedIndex = index;
    var eventRecord = eventsArray[index];
    document.getElementById("eventname").value = eventRecord.eventname;
    document.getElementById("eventdescription").value = eventRecord.eventdescription;
    document.getElementById("submit").innerHTML = "Update";
}